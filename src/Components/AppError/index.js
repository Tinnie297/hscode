import React from 'react';
import {StatusBar, Text, View} from 'react-native';
import errorStyles from './styles';
import LottieView from 'lottie-react-native';
import PropTypes from 'prop-types';
import {Colors} from '../../Constants';
const AppError = ({text, lottieUrl}) => {
  return (
    <>
      <StatusBar barStyle="default" backgroundColor={Colors.white} />
      <View style={errorStyles.container}>
        {lottieUrl && (
          <LottieView
            autoPlay
            // progress={2000}
            source={lottieUrl}
          />
        )}
      </View>
      <View style={errorStyles.textContainer}>
        <Text style={errorStyles.text}>{text}</Text>
      </View>
    </>
  );
};
AppError.defaultProps = {
  text: '',
  lottieUrl: require('../../assets/imgs/noData.json'),
};
AppError.propTypes = {
  text: PropTypes.string,
  lottieUrl: PropTypes.instanceOf(Object),
};
export default AppError;
