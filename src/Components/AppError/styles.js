import {StyleSheet} from 'react-native';
import {Colors, fontSize} from '../../Constants';

const errorStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    alignItems: 'center',
  },
  text: {
    fontSize: fontSize.large,
    fontWeight: '500',
    color: Colors.dark,
    textAlign: 'center',
  },
  textContainer: {
    backgroundColor: Colors.white,
  },
});

export default errorStyles;
