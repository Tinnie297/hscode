import {Dimensions, StyleSheet} from 'react-native';
import {Colors, fontSize} from '../../Constants';
import {pxScale} from '../../Helpers';

const {width: SCREEN_WIDTH} = Dimensions.get('window');
const width = pxScale.wp(327);
const height = pxScale.hp(45);

const AppTextInputStyles = StyleSheet.create({
  container: {
    width: SCREEN_WIDTH,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 10,
  },
  textInputWrapper: {
    height,
    width,
    borderRadius: height / 4,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.gray,
  },
  iconWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textInput: {
    height,
    width: width - 50,
    // backgroundColor: 'red',
    fontSize: fontSize.medium,
  },
  numberText: {
    fontWeight: '400',
    color: Colors.white,
    fontSize: fontSize.small,
  },
});

export default AppTextInputStyles;
