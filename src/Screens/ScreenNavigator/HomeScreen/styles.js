import {Dimensions, StyleSheet} from 'react-native';
import {Colors, fontSize} from '../../../Constants';

const {width: SCREEN_WIDTH} = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.primary,
  },
  itemContainer: {
    width: SCREEN_WIDTH - 30,
    height: 190,
    backgroundColor: Colors.white,
    borderRadius: 10,
    padding: 15,
    marginBottom: 10,
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  textDescriptionVN: {
    color: Colors.dark,
    flexWrap: 'wrap',
    paddingTop: 5,
  },
  textDescriptionEN: {
    color: Colors.dark,
    flexWrap: 'wrap',
    paddingTop: 5,
    fontStyle: 'italic',
  },
  textTitle: {
    fontSize: fontSize.medium,
    fontWeight: '600',
    paddingLeft: 10,
    color: Colors.highBlue,
  },
  textFooter: {
    // backgroundColor: 'red',
    textAlign: 'right',
    paddingTop: 5,
    color: Colors.highBlue,
    fontWeight: '600',
  },
  flexDirectionRow: {
    flexDirection: 'row',
  },
});
