import React from 'react';
import {
  FlatList,
  SafeAreaView,
  StatusBar,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import AppTextInput from '../../../Components/AppTextInput';
import AntDesign from 'react-native-vector-icons/AntDesign';
import styles from './styles';
import {Colors, iconSize} from '../../../Constants';
import {removeAccents} from '../../../Helpers';
import {useState} from 'react';
import AppError from '../../../Components/AppError';

const json = [
  {
    code: '0210.99',
    descriptionVN:
      'Thịt gà cắt miếng (thịt gà loài Gallus domesticus): đã được ngâm tẩm hoặc tiêm muối thực phẩm và được làm đông lạnh toàn bộ, với một hàm lượng muối từ 1.2% đến 3% tính theo trọng lượng, dùng làm thực phẩm cho người.Áp dụng quy tắc 1 và 6.',
    descriptionEN:
      'Chicken cuts (meat of fowls of the species Gallus domesticus), impregnated or injected with table salt in all their parts and frozen throughout, with a salt content of 1.2% or more but not more than 3%, by weight, suitable for human consumption. Application of GIRs 1 and 6.',
  },
  {
    code: '0303.82',
    descriptionVN:
      'Cánh cá đuối (thuộc giống Raja): được lấy từ thân bên trái và phải của cá đuối. Các cánh này không có da và được làm đông lạnh, gồm cả phần khối sụn có tỷ trọng thịt khoảng 86% và sụn là 14%.Áp dụng quy tắc 1 và 6.',
    descriptionEN:
      "Skate wings (of the genus Raja): obtained from the left and right sides of a skate's body. These wings, which are presented without skin and are frozen, include radial cartilage in a weight ratio of approximately 86% of meat to 14% of cartilage. Application of GIRs 1 and 6.",
  },
  {
    code: '0303.91',
    descriptionVN:
      'Sẹ cá trứng: được làm đông lạnh ở -18*C với hàm lượng muối không vượt quá 1% tính theo trọng lượng, ở dạng tảng từ 6kg đến 12kg. Sản phẩm này cần được chế biến thêm trước khi ăn. Áp dụng quy tắc 1 và 6.',
    descriptionEN:
      'Capelin roe: frozen at - 18*C with a salt content not exceeding 1% by weight, presented in blocks of 6kg to 12kg. The product is intended for further processing before consumption. Application of GIRs 1 and 6.',
  },
  {
    code: '0303.91',
    descriptionVN:
      'Sẹ cá trứng: được làm đông lạnh ở -18*C với hàm lượng muối không vượt quá 1% tính theo trọng lượng, ở dạng tảng từ 6kg đến 12kg. Sản phẩm này cần được chế biến thêm trước khi ăn. Áp dụng quy tắc 1 và 6.',
    descriptionEN:
      'Capelin roe: frozen at - 18*C with a salt content not exceeding 1% by weight, presented in blocks of 6kg to 12kg. The product is intended for further processing before consumption. Application of GIRs 1 and 6.',
  },
  {
    code: '0303.91',
    descriptionVN:
      'Sẹ cá trứng: được làm đông lạnh ở -18*C với hàm lượng muối không vượt quá 1% tính theo trọng lượng, ở dạng tảng từ 6kg đến 12kg. Sản phẩm này cần được chế biến thêm trước khi ăn. Áp dụng quy tắc 1 và 6.',
    descriptionEN:
      'Capelin roe: frozen at - 18*C with a salt content not exceeding 1% by weight, presented in blocks of 6kg to 12kg. The product is intended for further processing before consumption. Application of GIRs 1 and 6.',
  },
];

const HomeScreen = () => {
  const [data, setData] = useState(json);

  const [arrayholder, setArrayHoder] = React.useState(json);

  const getSearchText = React.useCallback(
    text => {
      const newData = arrayholder.filter(item => {
        const itemData = removeAccents(
          `${item.code.toUpperCase().trim()}` +
            `${item.descriptionVN.toUpperCase().trim()}` +
            `${item.descriptionEN.toUpperCase().trim()}`,
        );

        const textData = removeAccents(text.toUpperCase().trim());

        return itemData.includes(textData);
      });
      setData(newData);
    },
    [arrayholder],
  );
  const renderItem = React.useCallback(({item, index}) => {
    return (
      <TouchableOpacity style={styles.itemContainer}>
        <View style={styles.flexDirectionRow}>
          <AntDesign
            name="barcode"
            color={Colors.highBlue}
            size={iconSize.medium}
          />
          <Text style={styles.textTitle}>{`Mã hàng: ${item.code}`}</Text>
        </View>
        <Text numberOfLines={3} style={styles.textDescriptionVN}>
          {`Mô tả tiếng việt: ${item.descriptionVN}`}
        </Text>
        <Text numberOfLines={3} style={styles.textDescriptionEN}>
          {`Mô tả tiếng anh: ${item.descriptionEN}`}
        </Text>
        <Text style={styles.textFooter}>Xem chi tiết</Text>
      </TouchableOpacity>
    );
  }, []);
  return (
    <>
      <StatusBar barStyle="light-content" />
      <SafeAreaView style={styles.container}>
        <AppTextInput
          placeholder="Tìm kiếm"
          IconComponent={AntDesign}
          iconName="search1"
          onChangeText={getSearchText}
        />
        {!data.length && <AppError />}
        {data.length > 0 && (
          <FlatList
            data={data}
            keyExtractor={(_, index) => index.toString()}
            showsHorizontalScrollIndicator={false}
            contentContainerStyle={styles.center}
            renderItem={renderItem}
          />
        )}
      </SafeAreaView>
    </>
  );
};
export default HomeScreen;
