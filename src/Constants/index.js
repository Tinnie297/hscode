// import 'intl';
// import 'intl/locale-data/jsonp/vi-VN';
import {pxScale} from '../Helpers';

export const Colors = {
  purple: '#4E64C5',
  green: '#6CCFA9',
  yellow: '#f1c40f',
  brown: '#37173A',
  gray: '#F5F6FB',
  silver: '#bdc3c7',
  dark: '#696A6D',
  white: 'white',
  light: '#D3D3D3',
  // primary: '#4F6AFB',
  highBlue: '#38ada9',
  black: '#030303',
  blue: '#0059B8',
  primary: '#314566',
  red: '#e55039',
};

export const fontSize = {
  large: pxScale.fontSize(20),
  medium: pxScale.fontSize(16),
  small: pxScale.fontSize(14),
  detail: pxScale.fontSize(10),
};

export const iconSize = {
  large: 30,
  medium: 24,
  small: 16,
  detail: 12,
};

export const asyncStoreKey = {
  loginUser: 'loginUser',
};

// export const formatter = Intl.NumberFormat('vi-VN', {
//   style: 'currency',
//   currency: 'VND',
//   currencyDisplay: 'code',
// });
