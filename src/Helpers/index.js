import {Alert, Platform, Dimensions} from 'react-native';
import {isIphoneX} from 'react-native-iphone-x-helper';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import {RFValue} from 'react-native-responsive-fontsize';

// import axios from './axios';
// import {countries} from '../constants';

const {width: SCREEN_WIDTH, height: SCREEN_HEIGHT} = Dimensions.get('window');

export const isIos = Platform.OS === 'ios';

export const pxScale = {
  guidelineBaseWidth: 375,
  guidelineBaseHeight: 667, // iphone 8
  widthScale() {
    return SCREEN_WIDTH / this.guidelineBaseWidth;
  },
  heightScale() {
    return SCREEN_HEIGHT / this.guidelineBaseHeight;
  },

  wp(px) {
    const percentage = (px / this.guidelineBaseWidth) * 100;

    return wp(percentage);
  },

  hp(px) {
    const scale = isIphoneX() ? this.heightScale() : 1;

    const percentage = (px / scale / this.guidelineBaseHeight) * 100;

    return hp(percentage);
  },

  fontSize(px, baseHeight = isIphoneX() ? 812 : this.guidelineBaseHeight) {
    return RFValue(px, baseHeight);
  },
};

export const timezone = isIos
  ? Intl.DateTimeFormat().resolvedOptions().timeZone
  : '';

// export const getPhoneNumberLength = (countryCode = countries[0].value) => {
//   if (countryCode === 'au') {
//     return 10;
//   }
//   // Vietnam
//   return 10;
// };

// export const getFormattedPhoneNumber = phoneNumber => {
//   const countryIndex = countries.findIndex(c => phoneNumber.includes(c.code));

//   let phone = phoneNumber.replace(countries[countryIndex].code, '');
//   phone = `${countries[countryIndex].code} ${phone.slice(0, 4)} ${phone.slice(
//     4,
//   )}`;
//   return phone;
// };

export const uuid = () =>
  'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'
    .replace(/[xy]/g, c => {
      // eslint-disable-next-line no-bitwise
      const r = (Math.random() * 16) | 0;
      // eslint-disable-next-line no-bitwise, no-mixed-operators
      const v = c === 'x' ? r : (r & 0x3) | 0x8;
      return v.toString(16);
    })
    .toLowerCase();

export const apiErrorHandler = error => {
  let title = '';
  let message = 'Something wrong';

  if (error.response) {
    title = error.response.data.title;
    message = error.response.data.message;
  } else if (error.request) {
    message = error.request._response;
  } else {
    message = error.message;
  }

  Alert.alert(title, message);
};

// export const s3FileUploader = async (file) =>
//   new Promise((resolve, reject) => {
//     // Generate AWS S3 file url
//     axios.get(`/s3/sign?type=${file.type}`).then((uploadUrlResponse) => {
//       const {urlUploadFile} = uploadUrlResponse.data;

//       // eslint-disable-next-line no-undef
//       const xhr = new XMLHttpRequest();

//       xhr.onerror = reject;

//       xhr.onreadystatechange = () => {
//         if (xhr.readyState === 4 && xhr.status === 200) {
//           const splittedUrl = urlUploadFile.slice(
//             0,
//             urlUploadFile.indexOf('?'),
//           );
//           resolve(splittedUrl);
//         }
//       };

//       xhr.open('PUT', urlUploadFile);
//       xhr.setRequestHeader('X-Amz-ACL', 'public-read');
//       // for text file: text/plain, for binary file: application/octet-stream
//       xhr.setRequestHeader('Content-Type', file.type);
//       xhr.send(file);
//     });
//   });

export const getRequestIdByChannelName = channelName => {
  const arr = channelName.split('_');
  const requestId = arr[arr.length - 1];

  return requestId;
};

/**
 * Chuyển chữ có dấu sang không dấu
 */
export const removeAccents = str => {
  return str
    .normalize('NFD')
    .replace(/[\u0300-\u036f]/g, '')
    .replace(/đ/g, 'd')
    .replace(/Đ/g, 'D');
};
