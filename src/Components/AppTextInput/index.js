import React from 'react';
import {View, TextInput} from 'react-native';
import AppTextInputStyles from './styles';
import {Colors, iconSize} from '../../Constants';
import PropTypes from 'prop-types';
const AppTextInput = ({
  IconComponent,
  iconName,
  onChangeText,
  isRequired,
  placeholder,
  secureTextEntry,
  styleInput,
  styleContainer,
  onSubmitEditing,
  innerRef,
}) => {
  return (
    <View style={[AppTextInputStyles.container, styleContainer]}>
      <View style={AppTextInputStyles.textInputWrapper}>
        {IconComponent && (
          <View style={AppTextInputStyles.iconWrapper}>
            <IconComponent
              name={iconName}
              size={iconSize.medium}
              color={Colors.dark}
            />
          </View>
        )}
        <TextInput
          ref={innerRef}
          autoCorrect={false}
          autoCapitalize="none"
          // blurOnSubmit={false}
          placeholder={placeholder}
          onChangeText={onChangeText}
          secureTextEntry={secureTextEntry}
          onSubmitEditing={onSubmitEditing}
          placeholderTextColor={Colors.dark}
          style={[AppTextInputStyles.textInput, styleInput]}
        />
      </View>
    </View>
  );
};

AppTextInput.propTypes = {
  placeholder: PropTypes.string,
  iconName: PropTypes.string,
  secureTextEntry: PropTypes.bool,
  isRequired: PropTypes.bool,
  styleInput: PropTypes.instanceOf(Object),
  IconComponent: PropTypes.instanceOf(Object),
  styleContainer: PropTypes.instanceOf(Object),
};

AppTextInput.defaultProps = {
  placeholder: 'Tên đăng nhập',
  iconName: 'user',
  secureTextEntry: false,
};

export default AppTextInput;
